# vSphere Objects
variable "vcenter_username" {
  type    = string
  description = "The username for authenticating to vCenter."
  default = ""
}

variable "vcenter_password" {
  type    = string
  description = "The plaintext password for authenticating to vCenter."
  default = ""
}

variable "vcenter_insecure_connection" {
  type    = bool
  description = "If true, does not validate the vCenter server's TLS certificate."
  default = true
}

variable "vcenter_server" {
  type    = string
  description = "The fully qualified domain name or IP address of the vCenter Server instance."
  default = ""
}

variable "vcenter_datacenter" {
  type    = string
  description = "Required if there is more than one datacenter in vCenter."
  default = ""
}

variable "vcenter_host" {
  type = string
  description = "The ESXi host where target VM is created."
  default = ""
}

variable "vcenter_datastore" {
  type    = string
  description = "Required for clusters, or if the target host has multiple datastores."
  default = ""
}

variable "vm_folder" {
  type    = string
  description = "The VM folder in which the VM template will be created."
  default = ""
}

# Virtual Machine Networking
variable "vcenter_network" {
  type    = string
  description = "The network segment or port group name to which the primary virtual network adapter will be connected."
  default = ""
}

# Virtual Machine Credentials
variable "ssh_username" {
  type    = string
  description = "The username to use to authenticate over SSH."
  default = ""
}

variable "ssh_password" {
  type    = string
  description = "The plaintext password to use to authenticate over SSH."
  default = ""
}

# ISO Objects
variable "base_template" {
  type    = string
  description = "The base template used to create the VM."
  default = ""
}

# Virtual Machine Settings
variable "vm_cpu_sockets" {
  type = number
  description = "The number of virtual CPUs sockets."
  default = "1"
}

variable "vm_cpu_cores" {
  type = number
  description = "The number of virtual CPUs cores per socket."
}

variable "vm_mem_size" {
  type = number
  description = "The size for the virtual memory in MB."
}

variable "vm_disk_size" {
  type = number
  description = "The size for the virtual disk in MB."
}

variable "vm_name" {
  type = string
  description = "The name of the VM."
  default = ""
}

# Virtual Machine Configuration
variable "shell_scripts" {
  type = list(string)
  description = "A list of scripts."
  default = []
}