##################################################################################
# VARIABLES
##################################################################################
# Virtual Machine Settings
vm_name                     = "Ubuntu Server 21.04 Docker Template"
vm_folder                   = "Templates"
vm_cpu_cores                = 2
vm_mem_size                 = 4096
vm_disk_size                = 64000

# Credentials
ssh_username                = "ubuntu"
ssh_password                = "ubuntu"

# Template
base_template               = "Ubuntu Server 21.04 Template"

# Scripts
shell_scripts               = ["./scripts/generalize.sh"]
