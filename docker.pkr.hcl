##################################################################################
# LOCALS
##################################################################################
locals {
  buildtime = formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())
}

##################################################################################
# SOURCE
##################################################################################
source "vsphere-clone" "docker" {
  vcenter_server = var.vcenter_server
  insecure_connection = var.vcenter_insecure_connection
  username = var.vcenter_username
  password = var.vcenter_password
  host = var.vcenter_host

  template = var.base_template
  convert_to_template = true

  vm_name = var.vm_name
  folder = var.vm_folder

  RAM = var.vm_mem_size
  CPUs = var.vm_cpu_sockets
  cpu_cores = var.vm_cpu_cores
  disk_size = var.vm_disk_size
  notes = "Build: ${local.buildtime}"

  ip_wait_timeout = "20m"
  communicator = "ssh"
  ssh_password = var.ssh_password
  ssh_username = var.ssh_username
  ssh_port = 22
  ssh_timeout = "30m"
  ssh_handshake_attempts = "100000"
}


##################################################################################
# BUILD
##################################################################################
build {
  sources = [
    "source.vsphere-clone.docker"
  ]

  provisioner "ansible" {
    playbook_file = "./ansible/main.yml"
    user = "ubuntu"
    extra_arguments = ["--extra-vars",  "ansible_sudo_pass=${var.ssh_password}"]
  }

  provisioner "shell" {
    execute_command = "echo '${var.ssh_password}' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    environment_vars = [
      "BUILD_USERNAME=${var.ssh_username}",
    ]
    scripts = var.shell_scripts
    expect_disconnect = true
  }
}
